package dmonner.xlbp.stat;

import java.io.IOException;
import java.util.Map;

import dmonner.xlbp.util.CSVWriter;

public class FractionStat implements Stat
{
	private final String name;
	private int actual;
	private int possible;
	private float fraction;

	public FractionStat(final String name)
	{
		this.name = name;
	}

	public void add(final FractionStat that)
	{
		this.actual += that.actual;
		this.possible += that.possible;
	}

	public void add(final int actual, final int possible)
	{
		this.actual += actual;
		this.possible += possible;
	}

	@Override
	public void add(final Stat that)
	{
		if(that instanceof FractionStat)
			add((FractionStat) that);
		else
			throw new IllegalArgumentException("Can only add in other FractionStats.");
	}

	@Override
	public void addTo(final Map<String, Object> map)
	{
		map.put(name + "Fraction", fraction);
		map.put(name + "Actual", actual);
		map.put(name + "Possible", possible);
	}

	@Override
	public void analyze()
	{
		fraction = ((float) actual) / possible;
	}

	@Override
	public void clear()
	{
		actual = 0;
		possible = 0;
		fraction = 0;
	}

	public int getActual()
	{
		return actual;
	}

	public float getFraction()
	{
		return fraction;
	}

	public int getPossible()
	{
		return possible;
	}

	@Override
	public void saveData(final CSVWriter out) throws IOException
	{
		out.appendField(fraction);
		out.appendField(actual);
		out.appendField(possible);
	}

	@Override
	public void saveHeader(final CSVWriter out) throws IOException
	{
		out.appendHeader(name + "Fraction");
		out.appendHeader(name + "Actual");
		out.appendHeader(name + "Possible");
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append(name);
		sb.append("Fraction = ");
		sb.append(fraction);
		sb.append("\n");

		sb.append(name);
		sb.append("Actual = ");
		sb.append(actual);
		sb.append("\n");

		sb.append(name);
		sb.append("Possible = ");
		sb.append(possible);
		sb.append("\n");

		return sb.toString();
	}
}
