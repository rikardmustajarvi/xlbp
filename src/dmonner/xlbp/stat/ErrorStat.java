package dmonner.xlbp.stat;

import java.io.IOException;
import java.util.Map;

import dmonner.xlbp.util.CSVWriter;

public class ErrorStat implements Stat
{
	private final String name;
	private float sse;
	private float mse;
	private float rmse;
	private int n;

	public ErrorStat()
	{
		this("");
	}

	public ErrorStat(final String name)
	{
		this.name = name;
	}

	public void add(final ErrorStat that)
	{
		this.sse += that.sse;
		this.n += that.n;
	}

	@Override
	public void add(final Stat that)
	{
		if(that instanceof ErrorStat)
			add((ErrorStat) that);
		else
			throw new IllegalArgumentException("Can only add in other ErrorStats.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dmonner.xlbp.util.Stat#addTo(java.util.Map)
	 */
	@Override
	public void addTo(final Map<String, Object> map)
	{
		map.put(name + "N", n);
		map.put(name + "SSE", sse);
		map.put(name + "MSE", mse);
		map.put(name + "RMSE", rmse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dmonner.xlbp.util.Stat#analyze()
	 */
	@Override
	public void analyze()
	{
		mse = sse / n;
		rmse = (float) Math.sqrt(mse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dmonner.xlbp.util.Stat#clear()
	 */
	@Override
	public void clear()
	{
		sse = 0F;
		mse = 0F;
		rmse = 0F;
		n = 0;
	}

	public void compare(final float[] target, final float[] output)
	{
		for(int i = 0; i < target.length; i++)
		{
			final float diff = target[i] - output[i];
			sse += diff * diff;
		}

		n += target.length;
	}

	public float getMSE()
	{
		return mse;
	}

	public int getN()
	{
		return n;
	}

	public float getRMSE()
	{
		return rmse;
	}

	public float getSSE()
	{
		return sse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dmonner.xlbp.util.Stat#saveData(dmonner.xlbp.util.CSVWriter)
	 */
	@Override
	public void saveData(final CSVWriter out) throws IOException
	{
		out.appendField(n);
		out.appendField(sse);
		out.appendField(mse);
		out.appendField(rmse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dmonner.xlbp.util.Stat#saveHeader(dmonner.xlbp.util.CSVWriter)
	 */
	@Override
	public void saveHeader(final CSVWriter out) throws IOException
	{
		out.appendHeader(name + "N");
		out.appendHeader(name + "SSE");
		out.appendHeader(name + "MSE");
		out.appendHeader(name + "RMSE");
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append(name);
		sb.append("N = ");
		sb.append(n);
		sb.append("\n");

		sb.append(name);
		sb.append("SSE = ");
		sb.append(sse);
		sb.append("\n");

		sb.append(name);
		sb.append("MSE = ");
		sb.append(mse);
		sb.append("\n");

		sb.append(name);
		sb.append("RMSE = ");
		sb.append(rmse);
		sb.append("\n");

		return sb.toString();
	}

}
