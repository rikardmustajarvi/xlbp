package dmonner.xlbp.stat;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import dmonner.xlbp.layer.TargetLayer;
import dmonner.xlbp.trial.Step;
import dmonner.xlbp.util.CSVWriter;

public class StepStat implements Stat
{
	private final Step step;
	private final TargetSetStat targets;
	private final FractionStat correct;

	public StepStat(final Step step)
	{
		this.step = step;
		this.targets = new TargetSetStat();
		this.correct = new FractionStat("Step");

		analyze();
	}

	@Override
	public void add(final Stat that)
	{
		throw new IllegalArgumentException("Can only get data directly from a Step.");
	}

	@Override
	public void addTo(final Map<String, Object> map)
	{
		targets.addTo(map);
		correct.addTo(map);
	}

	@Override
	public void analyze()
	{
		for(final Entry<TargetLayer, float[]> entry : step.getTargets())
		{
			final TargetLayer layer = entry.getKey();
			final float[] target = entry.getValue();
			final TargetStat stat = new TargetStat(layer);
			stat.compare(target);
			stat.analyze();
			targets.add(stat);
		}

		targets.analyze();
		final int possible = targets.size() > 0 ? 1 : 0;
		final int actual = targets.getCorrect().getFraction() == 1F ? possible : 0;
		correct.add(actual, possible);
		correct.analyze();
	}

	@Override
	public void clear()
	{
		targets.clear();
		correct.clear();
	}

	public FractionStat getCorrect()
	{
		return correct;
	}

	public Step getStep()
	{
		return step;
	}

	public TargetSetStat getTargets()
	{
		return targets;
	}

	@Override
	public void saveData(final CSVWriter out) throws IOException
	{
		correct.saveData(out);
		targets.saveData(out);
	}

	@Override
	public void saveHeader(final CSVWriter out) throws IOException
	{
		targets.saveHeader(out);
		targets.saveHeader(out);
	}

	@Override
	public String toString()
	{
		final StringBuffer sb = new StringBuffer();

		sb.append(correct.toString());
		sb.append(targets.toString());

		return sb.toString();
	}

}
