package dmonner.xlbp.stat;

import java.io.IOException;
import java.util.Map;

import dmonner.xlbp.util.CSVWriter;

public class MeanVarStat implements Stat
{
	private final String name;
	private int sum;
	private int sumsq;
	private int n;
	private float mean;
	private float var;

	public MeanVarStat(final String name)
	{
		this.name = name;
	}

	public void add(final int obs)
	{
		sum += obs;
		sumsq += obs * obs;
		n++;
	}

	public void add(final MeanVarStat that)
	{
		sum += that.sum;
		sumsq += that.sumsq;
		n += that.n;
	}

	@Override
	public void add(final Stat that)
	{
		if(that instanceof MeanVarStat)
			add((MeanVarStat) that);
		else
			throw new IllegalArgumentException("Can only add in other MeanVarStats.");
	}

	@Override
	public void addTo(final Map<String, Object> map)
	{
		map.put(name + "Mean", mean);
		map.put(name + "Var", var);
	}

	@Override
	public void analyze()
	{
		mean = ((float) sum) / n;
		var = (sumsq - 2 * sum * mean) / n + mean * mean;
	}

	@Override
	public void clear()
	{
		sum = 0;
		sumsq = 0;
		n = 0;
		mean = 0;
		var = 0;
	}

	public float getMean()
	{
		return mean;
	}

	public float getVar()
	{
		return var;
	}

	@Override
	public void saveData(final CSVWriter out) throws IOException
	{
		out.appendField(mean);
		out.appendField(var);
	}

	@Override
	public void saveHeader(final CSVWriter out) throws IOException
	{
		out.appendHeader(name + "Mean");
		out.appendHeader(name + "Var");
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();

		sb.append(name);
		sb.append("Mean = ");
		sb.append(mean);
		sb.append("\n");

		sb.append(name);
		sb.append("Variance = ");
		sb.append(var);
		sb.append("\n");

		return sb.toString();
	}

}
