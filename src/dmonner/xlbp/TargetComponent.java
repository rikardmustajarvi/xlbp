package dmonner.xlbp;

public interface TargetComponent extends Component
{
	@Override
	public TargetComponent copy(String nameSuffix);

	@Override
	public TargetComponent copy(NetworkCopier copier);

	public void setTarget(final float[] activations);
}
