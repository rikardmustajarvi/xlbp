package dmonner.xlbp.trial;

import java.io.IOException;

import dmonner.xlbp.Network;
import dmonner.xlbp.stat.SetStat;
import dmonner.xlbp.stat.TrialStat;
import dmonner.xlbp.util.CSVWriter;

public class XValidationTrainer
{
	private final Network net;
	private final TrialStream stream;
	private TrialStat[][] evals;
	private TrialStat[][] bestEvals;
	private TrialRecord[][] records;
	private TrialRecord[][] bestRecords;
	private boolean keepEvaluations;
	private boolean keepRecords;
	private CSVWriter trainlog, testlog, validlog;
	private TrainingBreaker breaker;

	public XValidationTrainer(final TrialStream stream)
	{
		this.net = stream.getMetaNetwork();
		this.stream = stream;
		this.keepEvaluations = false;
		this.keepRecords = false;
	}

	private SetStat evaluateTest(final int fold, final int ep)
	{
		if(keepEvaluations)
			evals[fold] = new TrialStat[stream.nTestTrials()];

		final String time = "F" + fold + "-" + ep;
		final SetStat summary = new SetStat("Test" + time);

		for(int i = 0; i < stream.nTestTrials(); i++)
		{
			final Trial trial = stream.nextTestTrial();

			// Run the network on each test trial, while evaluating
			trial.setEvaluate(true);
			trial.setRecord(true);
			trial.run();

			summary.add(trial.getLastEvaluation());
			if(keepEvaluations)
				evals[fold][i] = trial.getLastEvaluation();

			postTestTrial(trial);
		}

		summary.analyze();
		return summary;
	}

	private SetStat evaluateTrain(final int fold, final int ep, final boolean train)
	{
		if(keepEvaluations)
			evals[fold] = new TrialStat[stream.nTrainTrials()];

		final String time = "F" + fold + "-" + ep;
		final SetStat summary = new SetStat("Train" + time);

		for(int i = 0; i < stream.nTrainTrials(); i++)
		{
			final Trial trial = stream.nextTrainTrial();

			// Run the network on each training trial, while evaluating
			trial.setEvaluate(true);
			trial.setRecord(true);
			trial.run(train);

			summary.add(trial.getLastEvaluation());
			if(keepEvaluations)
				evals[fold][i] = trial.getLastEvaluation();

			postTrainTrial(trial);
		}

		net.processBatch();

		summary.analyze();
		return summary;
	}

	private SetStat evaluateValid(final int fold, final int ep)
	{
		if(keepEvaluations)
			evals[fold] = new TrialStat[stream.nValidationTrials()];

		final String time = "F" + fold + "-" + ep;
		final SetStat summary = new SetStat("Valid" + time);

		for(int i = 0; i < stream.nValidationTrials(); i++)
		{
			final Trial trial = stream.nextValidationTrial();

			// Run the network on each training trial, while evaluating
			trial.setEvaluate(true);
			trial.setRecord(true);
			trial.run();

			summary.add(trial.getLastEvaluation());
			if(keepEvaluations)
				evals[fold][i] = trial.getLastEvaluation();

			postValidationTrial(trial);
		}

		summary.analyze();
		return summary;
	}

	public TrialStat[] getEvaluations()
	{
		// Calculate the total number of evaluationss
		int n = 0;
		for(int i = 0; i < bestEvals.length; i++)
			n += bestEvals[i].length;

		// Move them into a single temporary array
		final TrialStat[] all = new TrialStat[n];
		int t = 0;
		for(int i = 0; i < bestEvals.length; i++)
		{
			final TrialStat[] fold = bestEvals[i];
			for(int j = 0; j < fold.length; j++)
				all[t++] = fold[j];
		}

		return all;
	}

	public TrialRecord[] getRecords()
	{
		// Calculate the total number of records
		int n = 0;
		for(int i = 0; i < bestRecords.length; i++)
			n += bestRecords[i].length;

		// Move them into a single temporary array
		final TrialRecord[] all = new TrialRecord[n];
		int t = 0;
		for(int i = 0; i < bestRecords.length; i++)
		{
			final TrialRecord[] fold = bestRecords[i];
			for(int j = 0; j < fold.length; j++)
				all[t++] = fold[j];
		}

		return all;
	}

	private void log(final CSVWriter log, final SetStat summary, final int ep, final int fold)
	{
		if(log != null)
		{
			try
			{
				if(ep == 0 && fold == 0)
					summary.saveHeader(log);
				summary.saveData(log);
			}
			catch(final IOException ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void postEpoch(final int ep)
	{
		// Empty hook method.
	}

	public void postTestTrial(final Trial trial)
	{
		// Empty hook method.
	}

	public void postTrainTrial(final Trial trial)
	{
		// Empty hook method.
	}

	public void postValidationTrial(final Trial trial)
	{
		// Empty hook method.
	}

	public void preEpoch(final int ep)
	{
		// Empty hook method.
	}

	public void preFold(final int fold)
	{
		// Empty hook method.
	}

	public void preTest(final int fold)
	{
		// Empty hook method.
	}

	public void preTrain(final int fold)
	{
		// Empty hook method.
	}

	public SetStat run(final int maxEpochs)
	{
		final SetStat total = new SetStat(stream.getName());

		for(int f = 0; f < stream.nFolds(); f++)
		{
			// reinitialize the Network that is about to be trained
			net.rebuild();

			// run the training procedure for this fold and add up the results
			total.add(runFold(f, maxEpochs));
		}

		total.analyze();

		return total;
	}

	public SetStat runFold(final int fold, final int maxEpochs)
	{
		stream.setFold(fold);
		preFold(fold);

		// We will keep track of the best test set evaluation (for calibration), and the best validation
		// set evaluation (for reporting)
		SetStat best = null;
		SetStat bestTest = null;

		// Pre-training test
		log(trainlog, evaluateTrain(fold, 0, false), 0, fold);

		if(stream.nValidationFolds() > 0)
			log(validlog, evaluateValid(fold, 0), 0, fold);

		if(stream.nTestFolds() > 0)
			log(testlog, evaluateTest(fold, 0), 0, fold);

		// For each epoch
		for(int ep = 1; ep <= maxEpochs; ep++)
		{
			preEpoch(ep);
			preTrain(fold);

			// Train the network on the training set
			final SetStat trainSummary = evaluateTrain(fold, ep, true);
			log(trainlog, trainSummary, ep, fold);

			preTest(fold);

			// If we're using a validation set
			if(stream.nValidationFolds() > 0)
			{
				final SetStat testSummary;

				final SetStat validSummary = evaluateValid(fold, ep);
				log(validlog, validSummary, ep, fold);

				if(stream.nTestFolds() > 0)
				{
					// To evaluate the network on the test set, first create a new summary and note current
					// epoch
					testSummary = evaluateTest(fold, ep);
					log(testlog, testSummary, ep, fold);
				}
				else
				{
					testSummary = validSummary;
				}

				// Get the accuracy on the current evaluation and the best-to-date evaluation
				final float current = testSummary.getAccuracy();
				final float previous = bestTest == null ? -1F : bestTest.getAccuracy();

				// If the current epoch is better than the best seen so far, save it for later comparison
				if(current > previous)
				{
					bestTest = testSummary;
					best = validSummary;
					updateBest(fold);
				}

				// Stop training if current performance is too far below previous best
				if(breaker != null && breaker.isBreakTime(current))
					break;
			}
			// If we're not validating, but we are testing
			else if(stream.nTestFolds() > 0)
			{
				final SetStat testSummary = evaluateTest(fold, ep);
				log(testlog, testSummary, ep, fold);

				// Just keep track of the most recent test result to return
				best = testSummary;

				// Save the current test set evaluations; at the end of training we will have a
				// complete picture with a test-set result for each trial
				updateBest(fold);
			}
			else
			{
				best = trainSummary;
				updateBest(fold);
			}

			// Perform any post-epoch maintenance (like updating input values during an iterated
			// classification task
			postEpoch(ep);
		}

		return best;
	}

	public void setBreaker(final TrainingBreaker breaker)
	{
		this.breaker = breaker;
	}

	public void setKeepEvaluations(final boolean keepEvaluations)
	{
		this.keepEvaluations = keepEvaluations;

		if(keepEvaluations)
		{
			this.evals = new TrialStat[stream.nFolds()][];
			this.bestEvals = new TrialStat[stream.nFolds()][];
		}
		else
		{
			this.evals = null;
			this.bestEvals = null;
		}
	}

	public void setKeepRecords(final boolean keepRecords)
	{
		this.keepRecords = keepRecords;

		if(keepRecords)
		{
			this.records = new TrialRecord[stream.nFolds()][];
			this.bestRecords = new TrialRecord[stream.nFolds()][];
		}
		else
		{
			this.records = null;
			this.bestRecords = null;
		}
	}

	public void setTestLog(final CSVWriter log)
	{
		testlog = log;
	}

	public void setTrainLog(final CSVWriter log)
	{
		trainlog = log;
	}

	public void setValidationLog(final CSVWriter log)
	{
		validlog = log;
	}

	private void updateBest(final int fold)
	{
		// Save the current validation set evaluations; at the end of training we will have a
		// complete picture with a validation-set result for each trial
		if(keepEvaluations)
			bestEvals[fold] = evals[fold];

		if(keepRecords)
			bestRecords[fold] = records[fold];
	}
}
