package dmonner.xlbp.trial;

public interface TrainingBreaker
{
	public boolean isBreakTime(final float performance);
}
