package dmonner.xlbp.trial;

public abstract class AbstractTrialStream implements TrialStream
{
	protected static String makeSplitString(final int train, final int test, final int valid)
	{
		return train + "/" + test + "/" + valid;
	}

	private final int ntrain, ntest, nvalid, nfolds;

	public AbstractTrialStream(final int train, final int test, final int valid)
	{
		this(makeSplitString(train, test, valid));
	}

	public AbstractTrialStream(final String split)
	{
		final String[] fields = split.split("/");

		if(fields.length < 1 || fields.length > 3)
			throw new IllegalArgumentException("Need 1-3 fields in split string; found " + fields.length
					+ " in: " + split);

		try
		{
			this.ntrain = Integer.parseInt(fields[0]);
			this.ntest = fields.length > 1 ? Integer.parseInt(fields[1]) : 0;
			this.nvalid = fields.length > 2 ? Integer.parseInt(fields[2]) : 0;
		}
		catch(final NumberFormatException ex)
		{
			throw new IllegalArgumentException("Malformed split string; all entries must be numbers: "
					+ split, ex);
		}

		this.nfolds = ntrain + ntest + nvalid;

		if(ntrain + ntest + nvalid <= 0)
			throw new IllegalArgumentException("Need at least 1 fold.");
	}

	@Override
	public int nFolds()
	{
		return nfolds;
	}

	@Override
	public int nTestFolds()
	{
		return ntest;
	}

	@Override
	public int nTrainFolds()
	{
		return ntrain;
	}

	@Override
	public int nValidationFolds()
	{
		return nvalid;
	}
}
