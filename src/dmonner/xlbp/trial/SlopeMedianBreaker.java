package dmonner.xlbp.trial;

import dmonner.xlbp.util.ArrayQueue;
import dmonner.xlbp.util.IndexAwareHeapNode;
import dmonner.xlbp.util.SlidingMedian;

/**
 * Implements a sliding-median Theil-Sen slope estimator. This slope estimate is compared with the
 * minimum allowed slope at every step, and training is terminated if the slope falls below the
 * minimum.
 * 
 * @author dmonner
 */
public class SlopeMedianBreaker implements TrainingBreaker
{
	public static void main(final String[] args)
	{
		final SlopeMedianBreaker breaker = new SlopeMedianBreaker(0F, 10, 0);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(1F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(2F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(3F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(4F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(5F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(6F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(7F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(8F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(9F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(10F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(12F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(14F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(16F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(18F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(20F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(22F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(24F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(26F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(28F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(30F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(27F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(24F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(21F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(18F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(15F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(12F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(9F);
		System.out.println(breaker.medianSlope);
		breaker.isBreakTime(6F);
		System.out.println(breaker.medianSlope);
	}

	private final int wait;
	private int sofar;
	private final float minSlope;
	private final SlidingMedian medianSlope;
	private final ArrayQueue<Float> samples;
	private final ArrayQueue<ArrayQueue<IndexAwareHeapNode<Float>>> slopes;

	// TODO: add a "minimum" number of epochs before we break

	public SlopeMedianBreaker(final float minSlope, final int window, final int wait)
	{
		if(window <= 0)
			throw new IllegalArgumentException("Parameter window must be > 0.");

		this.minSlope = minSlope;
		this.wait = wait;

		// calculate the total number of slopes we have to store for this window size
		final int nSlopes = (window * (window - 1) / 2);

		// The default median slope
		this.medianSlope = new SlidingMedian(nSlopes);

		// Create and populate the samples queue with zeroes
		this.samples = new ArrayQueue<Float>(window);
		for(int i = 0; i < window; i++)
			this.samples.push(0F);

		// Create and populate the slopes matrix with zero Nodes from the heaps
		this.slopes = new ArrayQueue<ArrayQueue<IndexAwareHeapNode<Float>>>(window);
		int idx = 0;
		for(int i = 0; i < window; i++)
		{
			final ArrayQueue<IndexAwareHeapNode<Float>> slopeQ = new ArrayQueue<IndexAwareHeapNode<Float>>(
					window - 1);
			this.slopes.push(slopeQ);
			for(int j = i + 1; j < window; j++)
				slopeQ.push(this.medianSlope.getSample(idx++));
		}
	}

	@Override
	public boolean isBreakTime(final float performance)
	{
		// Take the outgoing slopes and remove them all from their various heaps
		final ArrayQueue<IndexAwareHeapNode<Float>> slopeQ = slopes.pop();

		// Remove the outgoing sample
		samples.pop();

		// Compute the incoming slopes and add them to the correct heaps, reusing slopeQ
		for(int i = 0; i < samples.size(); i++)
		{
			// calculate the new slope
			final float newSlope = (performance - samples.peek(i)) / (samples.size() - i);

			// figure out which slope to remove and which to add
			final IndexAwareHeapNode<Float> out = slopeQ.pop();
			final IndexAwareHeapNode<Float> in = new IndexAwareHeapNode<Float>(newSlope);

			// add the new slope to the slope matrix (so we can remove it in the correct order later)
			slopes.peek(i).push(in);

			// tell the sliding median calculator to add in and remove out
			medianSlope.update(in, out);
		}

		if(!slopeQ.isEmpty())
			throw new IllegalArgumentException("SHOULDNT HAPPEN!!!");

		// Add a (now empty) queue for incoming slopes to the master list
		slopes.push(slopeQ);

		// Add the incoming sample
		samples.push(performance);

		// If we haven't finished the minimum number of "wait" steps yet, we refuse to stop
		sofar++;
		if(sofar < wait)
			return false;

		System.out.println(sofar + ": medianSlope = " + medianSlope);

		// We now have the new correct medianSlope; compare against the minimum allowed slope.
		return medianSlope.get() < minSlope;
	}
}
