package dmonner.xlbp.trial;

import java.util.Random;

import dmonner.xlbp.Network;
import dmonner.xlbp.stat.MatrixTools;
import dmonner.xlbp.util.ArrayQueue;

public class TrialSet extends AbstractTrialStream
{
	private static int[] parseFoldSplit(final String foldSplit)
	{
		if(foldSplit.isEmpty())
			return new int[0];

		final String[] s = foldSplit.split("/");
		final int[] f = new int[s.length];

		try
		{
			for(int i = 0; i < s.length; i++)
				f[i] = Integer.parseInt(s[i].trim());
		}
		catch(final NumberFormatException ex)
		{
			throw new IllegalArgumentException(
					"Malformed foldSplit string; all entries must be numbers: " + foldSplit, ex);
		}

		return f;
	}

	private final String name;
	private final Network meta;
	private final Trial[][] folds;
	private Trial[] train, test, valid;
	private final ArrayQueue<Trial> trainCache, testCache, validCache;
	private final Random rand;

	public TrialSet(final String name, final Trial[] set, final int train, final int test,
			final int valid)
	{
		this(name, set, train, test, valid, new Random());
	}

	public TrialSet(final String name, final Trial[] set, final int train, final int test,
			final int valid, final Random random)
	{
		this(name, set, makeSplitString(train, test, valid), random);
	}

	public TrialSet(final String name, final Trial[] set, final int[] foldSizes, final int test,
			final int valid)
	{
		this(name, set, foldSizes, test, valid, new Random());
	}

	public TrialSet(final String name, final Trial[] set, final int[] foldSizes, final int test,
			final int valid, final Random random)
	{
		this(name, set, foldSizes, makeSplitString(foldSizes.length - test - valid, test, valid),
				random);
	}

	public TrialSet(final String name, final Trial[] set, final int[] foldSizes, final String split)
	{
		this(name, set, foldSizes, split, new Random());
	}

	public TrialSet(final String name, final Trial[] set, final int[] foldSizes, final String split,
			final Random random)
	{
		super(split);

		this.name = name;
		meta = set[0].getMetaNetwork();
		rand = random;
		trainCache = new ArrayQueue<Trial>();
		testCache = new ArrayQueue<Trial>();
		validCache = new ArrayQueue<Trial>();

		// If we have specified sizes for the folds
		if(foldSizes.length > 0)
		{
			if(foldSizes.length != nFolds())
				throw new IllegalArgumentException("Number of foldSizes entries (" + foldSizes.length
						+ ") does not match number of folds (" + nFolds() + ").");

			// Split the trials into folds based on their indices, as specified by foldSizes
			folds = new Trial[nFolds()][];
			int prev = 0;
			for(int f = 0; f < nFolds(); f++)
			{
				final int n = foldSizes[f];

				folds[f] = new Trial[n];
				for(int i = 0; i < n; i++)
					folds[f][i] = set[prev + i];

				prev += n;
			}
			// No need to randomize the folds; this gets done in setFold().
		}
		// Otherwise we should split up the folds evenly as best we can.
		else
		{
			final double fraction = ((double) set.length) / nFolds();

			// Randomize the trials
			MatrixTools.randomize(set, rand);

			// Split the trials into folds
			folds = new Trial[nFolds()][];
			for(int f = 0; f < nFolds(); f++)
			{
				final int start = (int) (f * fraction);
				final int end = (int) ((f + 1) * fraction);
				final int n = end - start;

				folds[f] = new Trial[n];
				for(int i = 0; i < n; i++)
					folds[f][i] = set[start + i];
			}
		}
	}

	public TrialSet(final String name, final Trial[] set, final String split)
	{
		this(name, set, "", split, new Random());
	}

	public TrialSet(final String name, final Trial[] set, final String split, final Random random)
	{
		this(name, set, "", split, random);
	}

	public TrialSet(final String name, final Trial[] set, final String foldSplit, final String split)
	{
		this(name, set, parseFoldSplit(foldSplit), split, new Random());
	}

	public TrialSet(final String name, final Trial[] set, final String foldSplit, final String split,
			final Random random)
	{
		this(name, set, parseFoldSplit(foldSplit), split, random);
	}

	@Override
	public Network getMetaNetwork()
	{
		return meta;
	}

	@Override
	public String getName()
	{
		return name;
	}

	public Trial getTestTrial(final int index)
	{
		return test[index];
	}

	public Trial getTrainTrial(final int index)
	{
		return train[index];
	}

	public Trial getValidationTrial(final int index)
	{
		return valid[index];
	}

	@Override
	public Trial nextTestTrial()
	{
		if(testCache.isEmpty())
			testCache.fill(test);

		return testCache.pop();
	}

	@Override
	public Trial nextTrainTrial()
	{
		if(trainCache.isEmpty())
			trainCache.fill(train);

		return trainCache.pop();
	}

	@Override
	public Trial nextValidationTrial()
	{
		if(validCache.isEmpty())
			validCache.fill(valid);

		return validCache.pop();
	}

	@Override
	public int nTestTrials()
	{
		return test.length;
	}

	@Override
	public int nTrainTrials()
	{
		return train.length;
	}

	@Override
	public int nValidationTrials()
	{
		return valid.length;
	}

	private Trial[] select(final int start, final int num)
	{
		// Calculate how many trials are being selected
		int n = 0;
		for(int i = 0; i < num; i++)
			n += folds[(start + i) % folds.length].length;

		// Move them into a single temporary array
		final Trial[] tr = new Trial[n];
		int t = 0;
		for(int i = 0; i < num; i++)
		{
			final Trial[] fold = folds[(start + i) % folds.length];
			for(int j = 0; j < fold.length; j++)
				tr[t++] = fold[j];
		}

		return tr;
	}

	private Trial[] selectTest(final int fold)
	{
		final Trial[] trials = select((fold + nTrainFolds()) % folds.length, nTestFolds());
		for(final Trial trial : trials)
			trial.setKnown(false);
		return trials;
	}

	private Trial[] selectTrain(final int fold)
	{
		final Trial[] trials = select(fold, nTrainFolds());
		for(final Trial trial : trials)
			trial.setKnown(true);
		return trials;
	}

	private Trial[] selectValidation(final int fold)
	{
		final Trial[] trials = select((fold + nTrainFolds() + nTestFolds()) % folds.length,
				nValidationFolds());
		for(final Trial trial : trials)
			trial.setKnown(false);
		return trials;
	}

	@Override
	public void setFold(final int fold)
	{
		train = selectTrain(fold);
		test = selectTest(fold);
		valid = selectValidation(fold);

		for(final Trial[] array : folds)
			for(final Trial trial : array)
				trial.regenerate();

		MatrixTools.randomize(train, rand);
		trainCache.fill(train);
		testCache.fill(test);
		validCache.fill(valid);
	}
}
