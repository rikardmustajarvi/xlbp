package dmonner.xlbp.trial;

public class FractionBestBreaker implements TrainingBreaker
{
	private final int wait;
	private int sofar;
	private final float fractionOfBest;
	private float bestPerformance;

	// TODO: add min wait

	public FractionBestBreaker(final float fractionOfBest, final int wait)
	{
		if(fractionOfBest <= 0F || fractionOfBest >= 1F)
			throw new IllegalArgumentException("Parameter fractionOfBest must be in (0, 1).");

		this.fractionOfBest = fractionOfBest;
		this.wait = wait;
		this.bestPerformance = Float.NEGATIVE_INFINITY;
	}

	@Override
	public boolean isBreakTime(final float performance)
	{
		if(performance > bestPerformance)
			bestPerformance = performance;

		// If we haven't finished the minimum number of "wait" steps yet, we refuse to stop
		sofar++;
		if(sofar < wait)
			return false;

		return performance < fractionOfBest * bestPerformance;
	}
}
